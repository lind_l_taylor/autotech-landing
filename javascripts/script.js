;$(function() {

  // прилепление хэдера
  var start_pos=$('#sticky .container').offset().top;
	 $(window).scroll(function(){
	  if ($(window).scrollTop()>=start_pos) {
	      if ($('#sticky').hasClass()==false) $('#sticky').addClass('navbar-fixed-top');
	  }
	  else $('#sticky').removeClass('navbar-fixed-top');
	 });


	// основной слайдер
	$('#topslider').owlCarousel({
	    loop:true,
	    mouseDrag:false,
	    touchDrag:true,
	    nav:true,
	    items: 1,
	    animateIn : "fadeIn",
	    animateOut : "fadeOut",
	    autoplay: true,
	    autoplayTimeout: 5000,
	    navText: [
	      "<",
	      ">"
	      ],
	})

	//слайдер проектов внизу
	$('#projslider').owlCarousel({
	    loop:true,
	    mouseDrag:false,
	    touchDrag:true,
	    nav:true,
	    items: 1,
	    animateIn : "fadeIn",
	    animateOut : "fadeOut",
	    autoplay: false,
	    autoplayTimeout: 5000,
	    navContainer: ".owl-controls",
	    navText: [
	      "<",
	      ">"
	      ],
	})




	ymaps.ready(init);
    var footerMap;

    function init(){     
        footerMap = new ymaps.Map("yandex_map", {
            center: [55.76, 37.64],
            zoom: 7
        });
    }


	$('#sticky').on('show.bs.collapse', function () {
	  // do something…
	  $('header, #sticky').addClass('darken');
	})
	$('#sticky').on('hide.bs.collapse', function () {
	  // do something…
	  $('header, #sticky').removeClass('darken');
	})

});

