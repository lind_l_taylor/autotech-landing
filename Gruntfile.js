module.exports = function(grunt) {

    // 1. All configuration goes here 
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            // 2. Configuration for concatinating files goes here.
            options: {
                separator: ';',
            },
            dist: {
                src: [
                    'javascripts/_bower.js',  // modules from bower
                    'javascripts/libs/*.js', // All JS in the libs folder
                    'javascripts/script.js'  // This specific file
                ],
                dest: 'javascripts/build/production.js',
            }
        },
        uglify: {
            build: {
                src: 'javascripts/build/production.js',
                dest: 'javascripts/build/production.min.js'
            }
        },
        sass: {
            dist: {
                options: {
                    style: 'compressed'
                },
                files: {
                    'stylesheets/styles.css': 'sass/styles.scss'
                }
            } 
        },
        cssmin: {
         options: {
            shorthandCompacting: false,
            roundingPrecision: -1
          },
          target: {
            files: [{
              expand: true,
              cwd: 'stylesheets',
              src: ['*.css', '!*.min.css', "!_*.css"],
              dest: 'stylesheets',
              ext: '.min.css'
            }]
          }
        },
        bower_concat: {
          all: {
            dest: 'javascripts/_bower.js',
            cssDest: 'stylesheets/_bower.css',
            bowerOptions: {
              relative: false
            },
            mainFiles: {
			  'freewall': 'freewall.js',
			}

          }
        },
        // concat_css: {  потом прикручу
        //     options: {
        //       // Task-specific options go here. 
        //     },
        //     all: {
        //       src: ["/**/*.css"],
        //       dest: "styles.css"
        //     },
        // },
        watch: {
            livereload: {
              // Here we watch the files the sass task will compile to
              // These files are sent to the live reload server after sass compiles to them
              options: { livereload: true },
              files: ['*.html'],
            },
            scripts: {
                files: ['javascripts/*.js'],
                tasks: ['concat', 'uglify'],
                options: {
                    livereload: true,
                    spawn: false,
                },
            },
            css: {
                files: ['sass/*.scss'],
                tasks: ['sass', 'cssmin'],
                options: {
                    livereload: true,
                    spawn: false,
                }
            }
        }
    });

    // 3. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-bower-concat');
    // grunt.loadNpmTasks('grunt-concat-css');
    grunt.loadNpmTasks('grunt-sass');

    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    // grunt.registerTask('default', ['concat', 'uglify', 'sass', 'watch']);
    
    // watch only
    grunt.registerTask('default', ['watch']);

    grunt.registerTask('buildbower', ['bower_concat', 'concat', 'uglify']);


};